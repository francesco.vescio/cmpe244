#include <stdio.h>

#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"

#include "adc.h"
#include "board_io.h"
#include "common_macros.h"
#include "delay.h"
#include "gpio.h"
#include "gpio_isr.h"
#include "gpio_lab.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include "periodic_scheduler.h"
#include "pwm1.h"
#include "semphr.h"
#include "sj2_cli.h"
#include "ssp2_lab.h"

#define SPI_LAB
#define SPI_LAB_PART_1

static void create_blinky_tasks(void);
static void create_uart_task(void);
static void blink_task(void *params);
static void uart_task(void *params);
static void task_one(void *task_parameter);
static void task_two(void *task_parameter);
#ifdef GPIO_LAB
static SemaphoreHandle_t switch_press_indication;
#endif
#ifdef INTERRUPTS_LAB
static void gpio_interrupt(void);
static SemaphoreHandle_t switch_pressed_signal;
static void sleep_on_sem_task(void *p);
static void pin29_isr(void);
static void pin30_isr(void);
#endif

#ifdef ADC_PWM_LAB
static QueueHandle_t adc_to_pwm_task_queue;

void pin_configure_pwm_channel_as_io_pin(void) { gpio__construct_with_function(GPIO__PORT_2, 0, GPIO__FUNCTION_1); }

void pwm_task(void *p) {
  pwm1__init_single_edge(1000);

  // Locate a GPIO pin that a PWM channel will control
  // NOTE You can use gpio__construct_with_function() API from gpio.h
  // TODO Write this function yourself
  pin_configure_pwm_channel_as_io_pin();

  // We only need to set PWM configuration once, and the HW will drive
  // the GPIO at 1000Hz, and control set its duty cycle to 50%
  pwm1__set_duty_cycle(PWM1__2_0, 50);

  // Continue to vary the duty cycle in the loop
  uint8_t percent = 0;

  int adc_reading = 0;

  while (1) {
    pwm1__set_duty_cycle(PWM1__2_0, percent);

#ifdef ADC_PWM_LAB_PART_0
    if (++percent > 100) {
      percent = 0;
    }
#endif

    if (xQueueReceive(adc_to_pwm_task_queue, &adc_reading, 100)) {
      percent = 100 * adc_reading / 4095;
      pwm1__set_duty_cycle(PWM1__2_0, percent);
    }
    fprintf(stderr, "MR0: %d | MR1: %d\n", LPC_PWM1->MR0, LPC_PWM1->MR1);
    // vTaskDelay(100);
  }
}

pin_configure_adc_channel_as_io_pin(void) { gpio__construct_with_function(GPIO__PORT_1, 31, GPIO__FUNCTION_3); }

void adc_task(void *p) {
  adc__initialize();

  // TODO This is the function you need to add to adc.h
  // You can configure burst mode for just the channel you are using
  adc__enable_burst_mode();

  // Configure a pin, such as P1.31 with FUNC 011 to route this pin as ADC channel 5
  // You can use gpio__construct_with_function() API from gpio.h
  pin_configure_adc_channel_as_io_pin(); // TODO You need to write this function
  int adc_reading = 0;
  while (1) {
    // Get the ADC reading using a new routine you created to read an ADC burst reading
    // TODO: You need to write the implementation of this function
    // const uint16_t adc_value = adc__get_channel_reading_with_burst_mode(ADC__CHANNEL_5);
    // Implement code to send potentiometer value on the queue
    // a) read ADC input to 'int adc_reading'
    // b) Send to queue: xQueueSend(adc_to_pwm_task_queue, &adc_reading, 0);
    adc_reading = adc__get_channel_reading_with_burst_mode(ADC__CHANNEL_5);
    if (!xQueueSend(adc_to_pwm_task_queue, &adc_reading, 0)) {
      fprintf(stderr, "ERROR: ADC not sent on queue");
    }
    float adc_voltage = 3.3f * adc_reading / 4095.0f;
    fprintf(stderr, "ADC Value: %f\n", adc_voltage);
    vTaskDelay(100);
  }
}
#endif

#ifdef SPI_LAB
static SemaphoreHandle_t spi_bus_mutex;

// TODO: Implement Adesto flash memory CS signal as a GPIO driver
static gpio_s flash_cs = {1, 10};
static gpio_s arbitrary_trigger = {0, 6};
void adesto_cs(void) {
  gpio__set_as_output(flash_cs);
  gpio__reset(flash_cs);

  gpio__set_as_output(arbitrary_trigger);
  gpio__reset(arbitrary_trigger);
}
void adesto_ds(void) {
  gpio__set(flash_cs);
  gpio__set(arbitrary_trigger);
}

void configure_ssp2_pin_functions(void) {
  gpio_s ssp2_sck = gpio__construct_with_function(GPIO__PORT_1, 0, GPIO__FUNCTION_4);  // SSP2_SCK
  gpio_s ssp2_miso = gpio__construct_with_function(GPIO__PORT_1, 4, GPIO__FUNCTION_4); // SSP2_MISO
  gpio_s ssp2_mosi = gpio__construct_with_function(GPIO__PORT_1, 1, GPIO__FUNCTION_4); // SSP2_MOSI

  gpio__set_as_output(ssp2_sck);
  gpio__set_as_output(ssp2_sck);
  gpio__set_as_output(ssp2_sck);
}

// TODO: Study the Adesto flash 'Manufacturer and Device ID' section
typedef struct {
  uint8_t manufacturer_id;
  uint8_t device_id_1;
  uint8_t device_id_2;
  uint8_t extended_device_id;
} adesto_flash_id_s;

// TODO: Implement the code to read Adesto flash memory signature
// TODO: Create struct of type 'adesto_flash_id_s' and return it
adesto_flash_id_s adesto_read_signature(void) {
  adesto_flash_id_s data;

  adesto_cs();
  {
    // Send opcode and read bytes
    uint8_t manufacturer_id_opcode = 0x9F;
    ssp2__trade_byte(manufacturer_id_opcode);
    // TODO: Populate members of the 'adesto_flash_id_s' struct
    data.manufacturer_id = ssp2__trade_byte(0xFF);
    data.device_id_1 = ssp2__trade_byte(0xFF);
    data.device_id_2 = ssp2__trade_byte(0xFF);
    data.extended_device_id = ssp2__trade_byte(0xFF);
  }
  adesto_ds();

  return data;
}

void spi_task(void *p) {
  const uint32_t spi_clock_mhz = 20;
  ssp2__init(spi_clock_mhz);

  // From the LPC schematics pdf, find the pin numbers connected to flash memory
  // Read table 84 from LPC User Manual and configure PIN functions for SPI2 pins
  // You can use gpio__construct_with_function() API from gpio.h
  //
  // Note: Configure only SCK2, MOSI2, MISO2.
  // CS will be a GPIO output pin(configure and setup direction)
  configure_ssp2_pin_functions();

  while (1) {
    adesto_flash_id_s id = adesto_read_signature();
    // TODO: printf the members of the 'adesto_flash_id_s' struct
    fprintf(stderr, "Manufacturer ID: %d\nDevice ID 1: %d\nDevice ID 2: %d\nExtended ID: %d\n", id.manufacturer_id,
            id.device_id_1, id.device_id_2, id.extended_device_id);
    vTaskDelay(500);
  }
}

void spi_id_verification_task(void *p) {
  while (1) {
    if (xSemaphoreTake(spi_bus_mutex, 1000)) {
      const adesto_flash_id_s id = adesto_read_signature();

      // When we read a manufacturer ID we do not expect, we will kill this task
      if (id.manufacturer_id != 0x1F) {
        fprintf(stderr, "Manufacturer ID read failure\n");
        vTaskSuspend(NULL); // Kill this task
      }
      xSemaphoreGive(spi_bus_mutex);
    }
  }
}
#endif
typedef struct __attribute__((packed)) {
  float f1; // 4 bytes
  char c1;  // 1 byte
  float f2;
  char c2;
} my_s;

#ifdef GPIO_LAB
typedef struct {
  /* First get gpio0 driver to work only, and if you finish it
   * you can do the extra credit to also make it work for other Ports
   */
  // uint8_t port;

  uint8_t pin;
} port_pin_s;

static void led_task(void *task_parameter) {
  // Type-cast the paramter that was passed from xTaskCreate()
  const port_pin_s *led = (port_pin_s *)(task_parameter);
  gpio1__set_as_output(led->pin);

  while (true) {
    gpio1__set_high(led->pin);
    vTaskDelay(100);

    gpio1__set_low(led->pin);
    vTaskDelay(100);
  }
}

static void switch_task(void *task_parameter) {
  const port_pin_s *_switch = (port_pin_s *)task_parameter;

  while (true) {
    // TODO: If switch pressed, set the binary semaphore
    if (gpio1__get_level(_switch->pin)) {
      xSemaphoreGive(switch_press_indication);
    }

    // Task should always sleep otherwise they will use 100% CPU
    // This task sleep also helps avoid spurious semaphore give during switch debeounce
    vTaskDelay(100);
  }
}

static void switch_led_task(void *task_parameter) {
  const port_pin_s *led = (port_pin_s *)(task_parameter);
  gpio1__set_as_output(led->pin);

  while (true) {
    // Note: There is no vTaskDelay() here, but we use sleep mechanism while waiting for the binary semaphore (signal)
    if (xSemaphoreTake(switch_press_indication, 1000)) {
      // TODO: Blink the LED
      gpio1__set_high(led->pin);
      vTaskDelay(100);

      gpio1__set_low(led->pin);
      vTaskDelay(100);
    } else {
      puts("Timeout: No switch press indication for 1000ms");
    }
  }
}

static void simple_led_task(void *pvParameters) {
  // Choose one of the onboard LEDS by looking into schematics and write code for the below
  // 0) Set the IOCON MUX function select pins to 000
  LPC_IOCON->P2_3 &= ~(7);
  // 1) Set the DIR register bit for the LED port pin
  LPC_GPIO2->DIR |= (1 << 3);
  while (true) {
    // 2) Set PIN register bit to 0 to turn ON LED (led may be active low)
    LPC_GPIO2->PIN &= ~(1 << 3);
    vTaskDelay(500);

    // 3) Set PIN register bit to 1 to turn OFF LED
    LPC_GPIO2->PIN |= (1 << 3);
    vTaskDelay(500);
  }
}
#endif

#ifdef INTERRUPTS_LAB_PART_2
static void pin29_isr(void) {
  fprintf(stderr, "Pin 29 ISR has run.");
  xSemaphoreGiveFromISR(switch_pressed_signal, NULL);
}

static void pin30_isr(void) {
  fprintf(stderr, "Pin 30 ISR has run.");
  xSemaphoreGiveFromISR(switch_pressed_signal, NULL);
}
#endif

/*FOR THE STRUCT HW
  The output without packing was:
    Size : 16 bytes
    floats 0x0x1000ffd0 0x0x1000ffd8
    chars  0x0x1000ffd4 0x0x1000ffdc
  The output with packing was:
    Size : 10 bytes
    floats 0x0x1000ffd4 0x0x1000ffd9
    chars  0x0x1000ffd8 0x0x1000ffdd

  Obviously they differ in their size because the padding is not present in the packed struct.
  Reading f2 and c2 from the packed struct will require finessing because the data isn't aligned neatly anymore
    */

int main(void) {
#ifdef GPIO_LAB
  // TODO:
  // Create two tasks using led_task() function
  // Pass each task its own parameter:
  // This is static such that these variables will be allocated in RAM and not go out of scope
  static port_pin_s led0 = {18};
  static port_pin_s led1 = {24};

  xTaskCreate(led_task, "led0", 2048 / sizeof(void *), &led0, PRIORITY_HIGH,
              NULL); /* &led0 is a task parameter going to led_task */
  xTaskCreate(led_task, "led1", 2048 / sizeof(void *), &led1, PRIORITY_LOW, NULL);
  xTaskCreate(simple_led_task, "simple_led", 2048 / sizeof(void *), NULL, PRIORITY_LOW, NULL);

  switch_press_indication = xSemaphoreCreateBinary();

  // Hint: Use on-board LEDs first to get this logic to work
  //       After that, you can simply switch these parameters to off-board LED and a switch
  static port_pin_s _switch = {19};
  static port_pin_s led3 = {26};

  xTaskCreate(switch_task, "switch", 2048 / sizeof(void *), &_switch, PRIORITY_MEDIUM, NULL);
  xTaskCreate(switch_led_task, "switch_led", 2048 / sizeof(void *), &led3, PRIORITY_MEDIUM, NULL);

#endif
#ifdef STRUCT_LAB
  create_blinky_tasks();
  create_uart_task();

  puts("Hello, World!");
  // TODO: Instantiate a struct of type my_s with the name of "s"
  my_s s;

  printf("Size : %d bytes\n"
         "floats 0x%p 0x%p\n"
         "chars  0x%p 0x%p\n",
         sizeof(s), &s.f1, &s.f2, &s.c1, &s.c2);
  puts("Starting RTOS");

  /**
   * Observe and explain the following scenarios:
   *
   * 1) Same Priority:      task_one = 1, task_two = 1
   * 2) Different Priority: task_one = 2, task_two = 1
   * 3) Different Priority: task_one = 1, task_two = 2
   *
   * Note: Priority levels are defined at FreeRTOSConfig.h
   * Higher number = higher priority
   *
   * Turn in screen shots of what you observed
   * as well as an explanation of what you observed
   */

  // Scenario 1
  // xTaskCreate(task_one, "task_one", 4096 / sizeof(void *), NULL, 1, NULL);
  // xTaskCreate(task_two, "task_two", 4096 / sizeof(void *), NULL, 1, NULL);

  // Scenario 2
  // xTaskCreate(task_one, "task_one", 4096 / sizeof(void *), NULL, 2, NULL);
  // xTaskCreate(task_two, "task_two", 4096 / sizeof(void *), NULL, 1, NULL);

  // Scenario 3
  xTaskCreate(task_one, "task_one", 4096 / sizeof(void *), NULL, 1, NULL);
  xTaskCreate(task_two, "task_two", 4096 / sizeof(void *), NULL, 2, NULL);

  vTaskStartScheduler(); // This function never returns unless RTOS scheduler runs out of memory and fails

  return 0;
#endif
#ifdef INTERRUPTS_LAB_PART_0
  // Read Table 95 in the LPC user manual and setup an interrupt on a switch connected to Port0 or Port2
  // a) For example, choose SW2 (P0_30) pin on SJ2 board and configure as input
  //.   Warning: P0.30, and P0.31 require pull-down resistors
  // b) Configure the registers to trigger Port0 interrupt (such as falling edge)
  LPC_IOCON->P0_30 &= ~(1 << 4);
  LPC_IOCON->P0_30 |= (1 << 3);
  LPC_GPIO0->DIR &= ~(1 << 30);
  LPC_GPIOINT->IO0IntEnF |= (1 << 30);
  // Install GPIO interrupt function at the CPU interrupt (exception) vector
  // c) Hijack the interrupt vector at interrupt_vector_table.c and have it call our gpio_interrupt()
  //    Hint: You can declare 'void gpio_interrupt(void)' at interrupt_vector_table.c such that it can see this function
  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__GPIO, gpio_interrupt, "gpio_interrupt");
  // Most important step: Enable the GPIO interrupt exception using the ARM Cortex M API (this is from lpc40xx.h)
  NVIC_EnableIRQ(GPIO_IRQn);

  // Toggle an LED in a loop to ensure/test that the interrupt is entering ane exiting
  // For example, if the GPIO interrupt gets stuck, this LED will stop blinking
  LPC_GPIO1->DIR |= (1 << 24);
  while (1) {
    delay__ms(100);
    // TODO: Toggle an LED here
    LPC_GPIO1->SET = (1 << 24);
    delay__ms(100);
    LPC_GPIO1->CLR = (1 << 24);
  }
#endif
#ifdef INTERRUPTS_LAB_PART_1
  switch_pressed_signal = xSemaphoreCreateBinary(); // Create your binary semaphore

  LPC_IOCON->P0_30 &= ~(1 << 4);
  LPC_IOCON->P0_30 |= (1 << 3);
  LPC_GPIO0->DIR &= ~(1 << 30);
  LPC_GPIOINT->IO0IntEnF |= (1 << 30);

  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__GPIO, gpio_interrupt,
                                   "gpio_interrupt"); // TODO: Setup interrupt by re-using code from Part 0
  NVIC_EnableIRQ(GPIO_IRQn);                          // Enable interrupt gate for the GPIO

  xTaskCreate(sleep_on_sem_task, "sem", (512U * 4) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
#endif
#ifdef INTERRUPTS_LAB_PART_2
  switch_pressed_signal = xSemaphoreCreateBinary();
  LPC_GPIO0->DIR &= ~(1 << 29);
  LPC_GPIO0->DIR &= ~(1 << 30);
  gpio0__attach_interrupt(29, GPIO_INTR__RISING_EDGE, pin29_isr);
  gpio0__attach_interrupt(30, GPIO_INTR__FALLING_EDGE, pin30_isr);

  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__GPIO, gpio0__interrupt_dispatcher, "isr_interrupt");
  NVIC_EnableIRQ(GPIO_IRQn);

  xTaskCreate(sleep_on_sem_task, "sem", (512U * 4) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
#endif
#ifdef ADC_PWM_LAB
  adc_to_pwm_task_queue = xQueueCreate(1, sizeof(int));

  xTaskCreate(adc_task, "adc", (512U * 4) / sizeof(void *), NULL, PRIORITY_HIGH, NULL);
  xTaskCreate(pwm_task, "pwm", (512U * 4) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
#endif
#ifdef SPI_LAB_PART_1
  xTaskCreate(spi_task, "spi", (512U * 4) / sizeof(void *), NULL, PRIORITY_HIGH, NULL);
#endif
#ifdef SPI_LAB_PART_2
  spi_bus_mutex = xSemaphoreCreateMutex();
  const uint32_t spi_clock_mhz = 24;
  ssp2__init(spi_clock_mhz);

  configure_ssp2_pin_functions();

  xTaskCreate(spi_id_verification_task, "spi1", (512U * 4) / sizeof(void *), NULL, PRIORITY_HIGH, NULL);
  xTaskCreate(spi_id_verification_task, "spi2", (512U * 4) / sizeof(void *), NULL, PRIORITY_HIGH, NULL);
#endif
  puts("Starting RTOS");
  vTaskStartScheduler(); // This function never returns unless RTOS scheduler runs out of memory and fails

  return 0;
}
#ifdef INTERRUPTS_LAB
void gpio_interrupt(void) {
  // a) Clear Port0/2 interrupt using CLR0 or CLR2 registers
  LPC_GPIOINT->IO0IntClr = (1 << 30);
  // b) Use fprintf(stderr) or blink and LED here to test your ISR
  fprintf(stderr, "ISR has run.");
  xSemaphoreGiveFromISR(switch_pressed_signal, NULL);
}

void sleep_on_sem_task(void *p) {
  LPC_GPIO1->DIR |= (1 << 24);
  while (1) {
    // Use xSemaphoreTake with forever delay and blink an LED when you get the signal
    if (xSemaphoreTakeFromISR(switch_pressed_signal, NULL)) {
      delay__ms(100);
      LPC_GPIO1->SET = (1 << 24);
      delay__ms(100);
      LPC_GPIO1->CLR = (1 << 24);
    }
  }
}

#endif
static void task_one(void *task_parameter) {
  while (true) {
    // Read existing main.c regarding when we should use fprintf(stderr...) in place of printf()
    // For this lab, we will use fprintf(stderr, ...)
    fprintf(stderr, "AAAAAAAAAAAA");

    // Sleep for 100ms
    vTaskDelay(100);
  }
}

static void task_two(void *task_parameter) {
  while (true) {
    fprintf(stderr, "bbbbbbbbbbbb");
    vTaskDelay(100);
  }
}

static void create_blinky_tasks(void) {
  /**
   * Use '#if (1)' if you wish to observe how two tasks can blink LEDs
   * Use '#if (0)' if you wish to use the 'periodic_scheduler.h' that will spawn 4 periodic tasks, one for each LED
   */
#if (1)
  // These variables should not go out of scope because the 'blink_task' will reference this memory
  static gpio_s led0, led1;

  led0 = board_io__get_led0();
  led1 = board_io__get_led1();

  xTaskCreate(blink_task, "led0", configMINIMAL_STACK_SIZE, (void *)&led0, PRIORITY_LOW, NULL);
  xTaskCreate(blink_task, "led1", configMINIMAL_STACK_SIZE, (void *)&led1, PRIORITY_LOW, NULL);
#else
  const bool run_1000hz = true;
  const size_t stack_size_bytes = 2048 / sizeof(void *); // RTOS stack size is in terms of 32-bits for ARM M4 32-bit CPU
  periodic_scheduler__initialize(stack_size_bytes, !run_1000hz); // Assuming we do not need the high rate 1000Hz task
  UNUSED(blink_task);
#endif
}

static void create_uart_task(void) {
  // It is advised to either run the uart_task, or the SJ2 command-line (CLI), but not both
  // Change '#if (0)' to '#if (1)' and vice versa to try it out
#if (0)
  // printf() takes more stack space, size this tasks' stack higher
  xTaskCreate(uart_task, "uart", (512U * 8) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
#else
  sj2_cli__init();
  UNUSED(uart_task); // uart_task is un-used in if we are doing cli init()
#endif
}

static void blink_task(void *params) {
  const gpio_s led = *((gpio_s *)params); // Parameter was input while calling xTaskCreate()

  // Warning: This task starts with very minimal stack, so do not use printf() API here to avoid stack overflow
  while (true) {
    gpio__toggle(led);
    vTaskDelay(500);
  }
}

// This sends periodic messages over printf() which uses system_calls.c to send them to UART0
static void uart_task(void *params) {
  TickType_t previous_tick = 0;
  TickType_t ticks = 0;

  while (true) {
    // This loop will repeat at precise task delay, even if the logic below takes variable amount of ticks
    vTaskDelayUntil(&previous_tick, 2000);

    /* Calls to fprintf(stderr, ...) uses polled UART driver, so this entire output will be fully
     * sent out before this function returns. See system_calls.c for actual implementation.
     *
     * Use this style print for:
     *  - Interrupts because you cannot use printf() inside an ISR
     *    This is because regular printf() leads down to xQueueSend() that might block
     *    but you cannot block inside an ISR hence the system might crash
     *  - During debugging in case system crashes before all output of printf() is sent
     */
    ticks = xTaskGetTickCount();
    fprintf(stderr, "%u: This is a polled version of printf used for debugging ... finished in", (unsigned)ticks);
    fprintf(stderr, " %lu ticks\n", (xTaskGetTickCount() - ticks));

    /* This deposits data to an outgoing queue and doesn't block the CPU
     * Data will be sent later, but this function would return earlier
     */
    ticks = xTaskGetTickCount();
    printf("This is a more efficient printf ... finished in");
    printf(" %lu ticks\n\n", (xTaskGetTickCount() - ticks));
  }
}
