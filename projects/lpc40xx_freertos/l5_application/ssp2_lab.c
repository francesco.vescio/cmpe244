#include "clock.h"
#include "lpc40xx.h"
#include "ssp2.h"

void ssp2__init(uint32_t max_clock_mhz) {
  // Refer to LPC User manual and setup the register bits correctly
  // a) Power on Peripheral
  uint32_t ssp2_power_on = (1 << 20);
  LPC_SC->PCONP |= ssp2_power_on;
  // b) Setup control registers CR0 and CR1
  uint8_t eight_bit_transfer = 0b111;
  uint8_t ssp_enable = (1 << 1);
  LPC_SSP2->CR0 |= eight_bit_transfer;
  LPC_SSP2->CR1 |= ssp_enable;
  // c) Setup prescalar register to be <= max_clock_mhz
  uint8_t prescale_divisor = 2; // minimum value
  uint32_t cpu_clock_mhz =
      clock__get_core_clock_hz() / 1000000; // this should always be 96 (didn't want to write magic number)
  uint8_t taret_divisor = cpu_clock_mhz / max_clock_mhz;

  while (prescale_divisor < taret_divisor && prescale_divisor < 254) {
    prescale_divisor += 2; // divisor must be even
  }

  LPC_SSP2->CPSR |= prescale_divisor;
}

uint8_t ssp2__trade_byte(uint8_t data_out) {
  // Configure the Data register(DR) to send and receive data by checking the SPI peripheral status register
  uint8_t transmit_not_full = (1 << 1);
  uint8_t busy_bit = (1 << 4);

  LPC_SSP2->DR = data_out;

  while (LPC_SSP2->SR & busy_bit) {
    ; // wait for bus activities to finish
  }

  uint8_t data_in = (uint8_t)LPC_SSP2->DR;
  return data_in;
}