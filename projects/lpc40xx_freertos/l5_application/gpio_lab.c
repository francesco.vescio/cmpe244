#pragma once
#include "gpio_lab.h"
#include "lpc40xx.h"

void gpio1__set_as_input(uint8_t pin_num) { LPC_GPIO1->DIR &= ~(1 << pin_num); }

void gpio1__set_as_output(uint8_t pin_num) { LPC_GPIO1->DIR |= (1 << pin_num); }

void gpio1__set_high(uint8_t pin_num) { LPC_GPIO1->SET = (1 << pin_num); }

void gpio1__set_low(uint8_t pin_num) { LPC_GPIO1->CLR = (1 << pin_num); }

void gpio1__set(uint8_t pin_num, bool high) {
  if (high) {
    LPC_GPIO1->SET = (1 << pin_num);
  } else {
    LPC_GPIO1->CLR = (1 << pin_num);
  }
}

bool gpio1__get_level(uint8_t pin_num) {
  if (LPC_GPIO1->PIN & (1 << pin_num)) {
    return true;
  } else {
    return false;
  }
}